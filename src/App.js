import React, { Component } from 'react';
import Navbar from './components/Navbar';
import Story from './components/Story';
import Filter from './components/Filter';
import Comment from './components/Comment';
import Loader from './components/Loader';
import Error from './components/Error';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      fetchError: false,
      data: [],
      search: 'search',
      tags: 'story',
      query:''
    }
  }

  fetchData = (search = "search", tags = "story") => {
    fetch(`https://hn.algolia.com/api/v1/${search}?tags=${tags}&query=${this.state.query}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({
          data: data.hits,
          loader: false,
        })
      })
      .catch((err) => {
        this.setState({
          fetchError: true,
          loader: false
        });
        console.log(err);
      })
  }

  componentDidMount() {
    this.fetchData();
  }

  changeFilter = (selectedOption) => {
    this.setState({
      tags: selectedOption,
      loader:true
    })
    this.fetchData(this.state.search, selectedOption);
  }

  changeFilterBy = (selectedOption) => {
    this.setState({
      search: selectedOption,
      loader:true
    })
    this.fetchData(selectedOption, this.state.tags);
  }

  searchValue = (value) =>{
    this.setState({
      query:value,
      loader:true
    })
    this.fetchData(this.state.search,this.state.tags);
  }

  render() {
    return (
      <>
        <Navbar searchValue={this.searchValue}/>
        <Filter changeFilter={this.changeFilter} changeFilterBy={this.changeFilterBy} />
        {
          this.state.loader ?
            <Loader /> :
            <></>
        }
        {
          this.state.fetchError === false ?
            this.state.data.map((story, index) => {
              return (
                <Story key={index} story={story} />
              )
            })
            :
            <>
              <Error />
            </>
        }
        {
          this.state.data.map((comment, index) => {
            return (
              <Comment key={index} comment={comment} />
            )
          })
        }
      </>
    )
  }
}

export default App;
