import React, { Component } from 'react';
import './Error.css';

class Error extends Component {
  render() {
    return (
      <div className='error-div'>
        <h1>Error in Loading stories</h1>
      </div>
    )
  }
}

export default Error;
