import React, { Component } from "react";
import './Navbar.css';
import logo from '../images/favicon.webp';
import img from '../images/logo-algolia-blue-35c461b6.svg';

class Navbar extends Component {
    onChange=(event)=>{
        this.props.searchValue(event.target.value);
    }
    render() {
        return (
            <nav>
                <div className="logo-div">
                    <img src={logo} alt="img" className="logo" />
                    <div className="logo-txt">
                        <p>Search</p>
                        <p>Hacker News</p>
                    </div>
                </div>
                <div className="search-box-div">
                    <i className="fa-solid fa-magnifying-glass"></i>
                    <input type="text" className="search-box" placeholder="Search stories by title, url or author" onChange={this.onChange}/>
                    <p>Search by</p> <img src={img} alt="img" />
                </div>
                <div className="settings-div">
                    <i className="fa-solid fa-gear"></i> 
                    <p>Settings</p>
                </div>
            </nav>
        )
    }
}

export default Navbar;