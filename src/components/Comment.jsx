import React, { Component } from 'react';
import moment from 'moment';
import './Comment.css';

class Comment extends Component {
    render() {
        return (
            this.props.comment.comment_text ?
                <div className='comment-div'>
                    <div className='comment-info'>
                        {
                            this.props.comment.points?
                            <span className='comment-text'><a href={`https://news.ycombinator.com/item?id=${this.props.comment.objectID}`}>{this.props.comment.points} points</a> | </span>
                            :
                            <></>
                        }
                        <span className='comment-text'><a href={`https://news.ycombinator.com/user?id=${this.props.comment.author}`}>{this.props.comment.author}</a> | </span>
                        <span className='comment-text'><a href={`https://news.ycombinator.com/item?id=${this.props.comment.objectID}`}>{moment(this.props.comment.created_at).fromNow()}</a> | </span>
                        <span className='comment-text'><a href={`https://news.ycombinator.com/item?id=${this.props.comment.parent_id}`}>parent</a> | </span>
                        {
                            this.props.comment.story_title ?
                            <span className='comment-text'>on : <a href={`https://news.ycombinator.com/item?id=${this.props.comment.parent_id}`}>{this.props.comment.story_title}</a></span>
                            :
                            <></>
                        }
                    </div>
                    <p>{this.props.comment.comment_text}</p>
                </div>
                :
                <></>
        )
    }
}

export default Comment;