import React, { Component } from "react";
import moment from "moment";
import './Story.css';

class Story extends Component {
    render() {
        return (
            this.props.story.title ?
                <div className="story-div">
                    <div className="title-div">
                        <span className="title"><a href={`https://news.ycombinator.com/item?id=${this.props.story.objectID}`}>{this.props.story.title} </a></span>
                        <span className="story-text"><a href={this.props.story.url}>({this.props.story.url})</a></span>
                    </div>
                    <div>
                        <span className="story-text"> <span className="title"><a href={`https://news.ycombinator.com/item?id=${this.props.story.objectID}`}>{this.props.story.points} points</a></span> |</span>
                        <span className="story-text"> <a href={`https://news.ycombinator.com/user?id=${this.props.story.author}`}>{this.props.story.author}</a> |</span>
                        <span className="story-text"> <span className="title"><a href={`https://news.ycombinator.com/item?id=${this.props.story.objectID}`}>{moment(this.props.story.created_at).fromNow()} </a></span> |</span>
                        <span className="story-text"> <span className="title"><a href={`https://news.ycombinator.com/item?id=${this.props.story.objectID}`}>{this.props.story.num_comments} comments</a></span></span>
                    </div>
                </div>
                :
                <>
                </>
        )
    }
}

export default Story;