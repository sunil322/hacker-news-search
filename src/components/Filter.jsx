import React, { Component } from "react";
import './Filter.css';

class Filter extends Component {

    changeFilter = (event) =>{    
        this.props.changeFilter(event.target.value);
    }
    changeFilterBy = (event) =>{
        this.props.changeFilterBy(event.target.value);
    }
    render() {
        return (
            <div className="filter-container">
                <div className="filter-div">
                    <span>Search</span>
                    <select onChange={this.changeFilter}>
                        <option value="story" className="filter-option">Stories</option>
                        <option value="comment" className="filter-option">Comments</option>
                    </select>
                    <span>by</span>
                    <select onChange={this.changeFilterBy}>
                        <option value="search" className="filter-option">Popularity</option>
                        <option value="search_by_date" className="filter-option">Date</option>
                    </select>
                </div>
                <div className="shares-div">
                    <p>32,419,114 results (0.003 seconds)</p>
                    <i className="fa-sharp fa-solid fa-share-nodes"></i>
                </div>
            </div>
        )
    }
}

export default Filter;